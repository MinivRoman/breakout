const roomService = require('./room.service');
const gameService = require('./game.service');

let connected = 0, room;

module.exports = (io) => {
  io.on('connection', socket => {
    connected++;
    io.emit('connected', { connected });
    
    socket.on('ready', ({ nickname }) => {
      const player = {
        socketId: socket.id,
        nickname
      };

      roomService.joinRoom(socket, player);
      room = roomService.getRoomBySocketId(socket.id);
      
      const playerIndex = room.playerList.length - 1;
      const gameConfig = gameService.getGameConfigForPlayer(playerIndex);

      io.to(socket.id).emit('initGameConfig', { gameConfig });
      
      if (!roomService.isFreeRoom(room)) {
        io.to(room.number).emit('startGame');
        gameService.runGameLoop(io, room);
      }
    });
    socket.on('movePaddle', payload => {
      const paddleX = gameService.getOpponentPaddle(payload);
      room && socket.broadcast.to(room.number).emit('movePaddle', { paddleX });
    });
    socket.on('moveBall', payload => {
      room && socket.broadcast.to(room.playerList[1].socketId).emit('moveBall', payload);
    });
    socket.on('disconnect', () => {
      roomService.leaveRoom(socket);
      connected--;
      io.emit('connected', { connected });
    });
  });
};
