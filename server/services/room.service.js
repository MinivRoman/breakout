class Room {
  constructor() {
    this.roomCounter = 0;
    this.list = [];
    this.maxRoomPlayers = 2;
  }
  joinRoom(socket, player) {
    if (!this.joinToFreeRoom(player)) {
      this.joinToNewRoom(player);
    }

    socket.join(this.getRoomBySocketId(socket.id).number);
  }
  joinToFreeRoom(player) {
    const freeRoom = this.list.find(this.isFreeRoom.bind(this));
    
    if (freeRoom) {
      freeRoom.playerList.push(player);
      return true;
    }
    return false;
  }
  isFreeRoom(room) {
    return room.playerList.length < this.maxRoomPlayers;
  }
  joinToNewRoom(player) {
    this.roomCounter++;

    this.list.push({
      number: this.roomCounter,
      playerList: [player]
    });
  }
  leaveRoom(socket) {
    const room = this.getRoomBySocketId(socket.id);

    if (room) {
      room.playerList = room.playerList.filter(player => player.socketId != socket.id);
      socket.leave(room.number);
    }
  }
  getRoomBySocketId(socketId) {
    return this.list.find(room => room.playerList.find(player => player.socketId == socketId));
  }
  getPlayerBySocketId(socketId) {
    return this.getRoomBySocketId(socketId).playerList.find(player => player.socketId == socketId);
  }
};

module.exports = new Room();
