const { getGameConfig } = require('../repositories/game.repository');
const { getCleanCopy } = require('../helpers/getCleanCopy');

class Game {
  constructor() {
    this.gameConfig = getGameConfig();
  }
  init() {
    this.gameConfig = getGameConfig();

    this.initCanvasCenter();
    this.initBrickArea();
    this.initPaddleList();
    this.initBall();

    this.gameConfig.state = 'play';
  }
  initCanvasCenter() {
    const { canvas } = this.gameConfig;

    canvas.center = {
      x: canvas.width / 2,
      y: canvas.height / 2
    };
  }
  initBrickArea() {
    const { canvas, brick } = this.gameConfig;
    const
      heightBrickArea = brick.rows * brick.height + (brick.rows - 1) * brick.padding,
      widthBrickArea = brick.columns * brick.width + (brick.columns - 1) * brick.padding;

    brick.offsetTop = (canvas.height - heightBrickArea) / 2;
    brick.offsetLeft = (canvas.width - widthBrickArea) / 2;

    for (let r = 0; r < brick.rows; r++) {
      brick.list[r] = [];
      for (let c = 0; c < brick.columns; c++) {
        brick.list[r][c] = { x: null, y: null, display: true };
      }
    }

    for (let r = 0; r < brick.rows; r++) {
      for (let c = 0; c < brick.columns; c++) {
        const brickX = brick.offsetLeft + c * (brick.width + brick.padding);
        const brickY = brick.offsetTop + r * (brick.height + brick.padding);

        brick.list[r][c].x = brickX;
        brick.list[r][c].y = brickY;
      }
    }
  }
  initPaddleList() {
    const { canvas, paddle } = this.gameConfig;

    const paddleX = (canvas.width - paddle.width) / 2;
    paddle.list[0] = { x: paddleX, y: canvas.height - paddle.height };
    paddle.list[1] = { x: paddleX, y: 0 };
  }
  initBall() {
    const { ball, paddle } = this.gameConfig;

    ball.x = paddle.list[0].x + paddle.width / 2;
    ball.y = paddle.list[0].y - ball.radius;
  }
  getGameConfigForPlayer(playerIndex) {

    this.init();

    const gameConfig = getCleanCopy(this.gameConfig);
    const { ball, brick } = gameConfig;

    if (playerIndex) {
      const ballPosition = this.getBallPositionForPlayer(playerIndex);

      ball.x = ballPosition.x;
      ball.y = ballPosition.y;

      brick.list = this.getBrickListForPlayer(playerIndex);
    }

    return gameConfig;
  }
  getOpponentPaddle({ paddleX }) {
    const { canvas, paddle } = this.gameConfig;

    paddle.list[0].x = paddleX;
    // symmetry axis Y
    paddle.list[1].x = canvas.width - paddle.width - paddleX;

    return paddle.list[1].x;
  }
  getBallPositionForPlayer(playerIndex) {
    const { canvas, ball } = this.gameConfig;
    const ballPosition = {};

    if (!playerIndex) {
      ballPosition.x = ball.x;
      ballPosition.y = ball.y;
    } else {
      // symmetry to center
      ballPosition.x = 2 * canvas.center.x - ball.x;
      ballPosition.y = 2 * canvas.center.y - ball.y;
    }

    return ballPosition;
  }
  getBrickListForPlayer(playerIndex) {
    const { brick } = this.gameConfig;
    const brickList = getCleanCopy(brick.list);

    // symmetry to center
    if (playerIndex) {
      for (let r = 0, rEnd = brick.rows - 1; r < brick.rows; r++ , rEnd--) {
        for (let c = 0, cEnd = brick.columns - 1; c < brick.columns; c++ , cEnd--) {
          brickList[rEnd][cEnd].display = brick.list[r][c].display;
        }
      }
    }

    return brickList;
  }
  runGameLoop(io, room) {
    if (this.gameConfig.state == 'play') {
      this.moveBall(io, room);
      this.collisionDetection();

      this.updateAnimationFrame(io, room);

      this.gameConfig.gameFrameID = setTimeout(this.runGameLoop.bind(this, io, room), 1000 / 60);
    }
  }
  moveBall(io, room) {
    const { canvas, ball, paddle } = this.gameConfig;

    if (ball.x + ball.stepX < ball.radius || ball.x + ball.stepX > canvas.width - ball.radius) {
      ball.stepX = -ball.stepX;
    }
    if (ball.y + ball.stepY < ball.radius + paddle.height) {
      if (ball.x > paddle.list[1].x && ball.x < paddle.list[1].x + paddle.width) {
        ball.stepY = -ball.stepY;
      } else {
        this.stop();
        this.sendResult(io, room, 0);
      }
    }
    else if (ball.y > canvas.height - ball.radius - paddle.height) {
      if (ball.x > paddle.list[0].x && ball.x < paddle.list[0].x + paddle.width) {
        ball.stepY = -ball.stepY;
      } else {
        this.stop();
        this.sendResult(io, room, 1);
      }
    }

    ball.x += ball.stepX;
    ball.y += ball.stepY;
  }
  collisionDetection() {
    if (!this.isBrokenAllBricks()) {
      const { ball, brick } = this.gameConfig;

      for (let r = 0; r < brick.rows; r++) {
        for (let c = 0; c < brick.columns; c++) {
          const currentBrick = brick.list[r][c];
          if (currentBrick.display) {
            if (
              ball.x + ball.radius > currentBrick.x && ball.x - ball.radius < currentBrick.x + brick.width &&
              ball.y + ball.radius > currentBrick.y && ball.y - ball.radius < currentBrick.y + brick.height
            ) {
              ball.stepY = -ball.stepY;
              currentBrick.display = false;
            }
          }
        }
      }
    } else {
      if (!this.gameConfig.highGameLevel.start) {
        this.increaseGameLevel();
      }
    }
  }
  isBrokenAllBricks() {
    const { brick } = this.gameConfig;
    return !brick.list.some(columns => columns.some(column => column.display));
  }
  increaseGameLevel() {
    const { highGameLevel, ball } = this.gameConfig;

    highGameLevel.timerId = setInterval(() => {
      ball.stepX += ball.stepX < 0 ? -ball.incraseStep : ball.incraseStep;
      ball.stepY += ball.stepY < 0 ? -ball.incraseStep : ball.incraseStep;
    }, highGameLevel.timeInterval * 1000);

    highGameLevel.start = true;
  }
  updateAnimationFrame(io, room) {
    room.playerList.forEach((player, index) => {
      const { socketId } = player;
      const ballPosition = this.getBallPositionForPlayer(index);
      const brickList = this.getBrickListForPlayer(index);

      io.to(socketId).emit('updateAnimationFrame', { ballPosition, brickList });
    });
  }
  sendResult(io, room, winnerIndex) {
    io.to(room.number).emit('result', {
      winner: room.playerList[winnerIndex].nickname,
      state: this.gameConfig.state
    });
  }
  stop() {
    clearInterval(this.gameConfig.highGameLevel.timerId);
    clearTimeout(this.gameConfig.gameFrameID);
    this.gameConfig.state = 'stop';
  }
};

module.exports = new Game();
