const express = require('express');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const socketService = require('./services/socket.service');

const SERVER_PORT = 5555;

server.listen(SERVER_PORT, () => {
  console.log(`Server is running on http://localhost:${SERVER_PORT}`);
  socketService(io);
});
