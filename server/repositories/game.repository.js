const fs = require('fs');

const getGameConfig = () => {
  const data = fs.readFileSync('./configs/game.config.json', 'utf8');
  return JSON.parse(data);
}

module.exports = {
  getGameConfig
};
