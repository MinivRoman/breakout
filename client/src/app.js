import io from 'socket.io-client';
import { setConnection } from './js/socket';
import Notification from './js/notification';
import Game from './js/game';
import './css/app.css';

const socket = io('http://localhost:5555');

const
  connected = document.getElementById('connected'),
  gameContainer = document.getElementById('game-container');

const game = new Game(gameContainer, socket);

setConnection(socket, connected, game, Notification);

Notification.getNickname(socket);
