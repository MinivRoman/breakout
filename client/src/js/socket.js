export function setConnection (socket, connectedElem, game, Notification) {
  socket.on('connected', ({ connected }) => {
    connectedElem.textContent = connected;
  });
  socket.on('initGameConfig', ({ gameConfig }) => {
    game.initGameConfig(gameConfig);
  });
  socket.on('startGame', () => {
    Notification.closeModalWindow();
  
    game.setUserEvents();
    game.render();
  });
  socket.on('movePaddle', payload => {
    game.setOpponentPaddle(payload);
  });
  socket.on('updateAnimationFrame', payload => {
    game.updateAnimationFrame(payload);
  });
  socket.on('result', ({ state, winner }) => {
    game.stop(state);
    Notification.showWinner(winner);
  }); 
};
