class Game {
  constructor(gameContainer, socket) {
    this.gameContainer = gameContainer;
    /** @type {CanvasRenderingContext2D} */
    this.ctx;
    this.gameConfig;
    this.socket = socket;
  }
  initGameConfig(gameConfig) {
    this.gameConfig = gameConfig;
    this.createCanvas();
  }
  createCanvas() {
    const { canvas } = this.gameConfig;

    canvas.element = document.createElement('canvas');
    canvas.element.setAttribute('id', 'breakout');
    canvas.element.setAttribute('width', `${canvas.width}px`);
    canvas.element.setAttribute('height', `${canvas.height}px`);
    this.gameContainer.appendChild(canvas.element);

    this.ctx = canvas.element.getContext('2d');
  }
  setOpponentPaddle({ paddleX }) {
    const { paddle } = this.gameConfig;
    paddle.list[1].x = paddleX;
  }
  updateAnimationFrame({ ballPosition, brickList }) {
    const { ball, brick } = this.gameConfig;

    ball.x = ballPosition.x;
    ball.y = ballPosition.y;

    brick.list = brickList;
  }
  render() {
    if (this.gameConfig.state == 'play') {
      this.clearCanvas();

      this.drawPaddle();
      this.drawBall();
      this.drawBricks();

      this.movePaddle();

      this.gameConfig.animationFrameID = requestAnimationFrame(this.render.bind(this));
    }
  }
  clearCanvas() {
    const { canvas } = this.gameConfig;
    this.ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  drawPaddle() {
    const { paddle, mainColor } = this.gameConfig;

    paddle.list.forEach(p => {
      this.ctx.beginPath();
      this.ctx.rect(p.x, p.y, paddle.width, paddle.height);
      this.ctx.fillStyle = mainColor;
      this.ctx.fill();
      this.ctx.closePath();
    });
  }
  drawBall() {
    const { ball, mainColor } = this.gameConfig;

    this.ctx.beginPath();
    this.ctx.arc(ball.x, ball.y, ball.radius, Math.PI * 2, false);
    this.ctx.fillStyle = mainColor;
    this.ctx.fill();
    this.ctx.closePath();
  }
  drawBricks() {
    const { brick, mainColor } = this.gameConfig;

    for (let r = 0; r < brick.rows; r++) {
      for (let c = 0; c < brick.columns; c++) {
        const { x, y, display } = brick.list[r][c];
        if (display) {
          this.ctx.beginPath();
          this.ctx.rect(x, y, brick.width, brick.height);
          this.ctx.fillStyle = mainColor;
          this.ctx.fill();
          this.ctx.closePath();
        }
      }
    }
  }
  movePaddle() {
    const { paddle, canvas } = this.gameConfig;

    if (this.gameConfig.keyPressed) {
      if (this.gameConfig.keyPressed == 'ArrowLeft' && paddle.list[0].x > 0) {
        paddle.list[0].x -= paddle.stepX;
      }
      else if (this.gameConfig.keyPressed == 'ArrowRight' && paddle.list[0].x < canvas.width - paddle.width) {
        paddle.list[0].x += paddle.stepX;
      }

      this.socket.emit('movePaddle', { paddleX: paddle.list[0].x });
    }
  }
  setUserEvents() {
    document.addEventListener('mousemove', this.mouseMoveHandler.bind(this), false);
    document.addEventListener('keydown', this.keyDownHandler.bind(this), false);
    document.addEventListener('keyup', this.keyUpHandler.bind(this), false);
  }
  mouseMoveHandler(e) {
    const { paddle, canvas } = this.gameConfig;

    const relativeX = e.clientX - canvas.element.offsetLeft;
    if (relativeX > paddle.width / 2 && relativeX < canvas.element.width - paddle.width / 2) {
      paddle.list[0].x = relativeX - paddle.width / 2;
    }

    this.socket.emit('movePaddle', { paddleX: paddle.list[0].x });
  }
  keyDownHandler(e) {
    const { key } = e;
    const { keyList } = this.gameConfig;

    if (keyList.includes(key)) {
      this.gameConfig.keyPressed = key;
    }
  }
  keyUpHandler(e) {
    this.gameConfig.keyPressed = null;
  }
  stop(state) {
    cancelAnimationFrame(this.gameConfig.animationFrameID);
    this.gameConfig.state = state;
  }
};

export default Game;
