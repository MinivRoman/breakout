import Swal from 'sweetalert2';

class Notification {
  static getNickname(socket) {
    Swal.fire({
      title: 'Enter your nickname',
      input: 'text',
      inputAttributes: {
        required: 'on'
      },
      validationMessage: 'Field cannot be blank!',
      confirmButtonText: 'ready',
      allowOutsideClick: false,
      allowEscapeKey: false
    }).then(result => {
      const { value: nickname } = result;
      socket.emit('ready', { nickname });
      
      Notification.showWaiting();
    });
  }
  static showWaiting() {
    Swal.fire({
      icon: 'info',
      title: 'Waiting for opponent...',
      showConfirmButton: false,
      allowOutsideClick: false,
      allowEscapeKey: false
    });
  }
  static showWinner(winner) {
    Swal.fire({
      'title': `Winner - ${winner}`
    });
  }
  static closeModalWindow() {
    Swal.close();
  }
};

export default Notification;
